import type { Meta, StoryObj } from '@storybook/svelte';
import Image from './Image.svelte';

const meta = {
	title: 'Image Component',
	component: Image,
	tags: ['autodocs'],
	argTypes: {
		type: {
			control: { type: 'select' },
			options: ['square', 'rectangular']
		}
	}
} satisfies Meta<Image>;

export default meta;
type Story = StoryObj<typeof meta>;

export const ImageComponent: Story = {
	args: {
		imageUrl:
			'https://images.unsplash.com/photo-1575936123452-b67c3203c357?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aW1hZ2V8ZW58MHx8MHx8fDA%3D&w=1000&q=80',
		title: 'Title',
		type: 'rectangular'
	}
};